package com.xing.hiring.marsrover.command;

import java.util.List;

import com.xing.hiring.marsrover.model.Rover;
import com.xing.hiring.marsrover.model.RoverDriver;

public class Commander {
	
	private RoverDriver roverDriver;
	private List<ICommand> commands;
	
	public void setRoverDriver (RoverDriver roverDriver) {
		this.roverDriver = roverDriver;
	}
	
	public void setCommands (List<ICommand> commands) {
		this.commands = commands;
	}
	
	public void runCommands () {
		for (ICommand command : this.commands) {
			command.execute(roverDriver);
		}
	}
	
	public Rover getRover() {
		return this.roverDriver.getRover();
	}

}
