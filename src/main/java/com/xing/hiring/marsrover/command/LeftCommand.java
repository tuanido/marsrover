package com.xing.hiring.marsrover.command;

import com.xing.hiring.marsrover.model.RoverDriver;

public class LeftCommand implements ICommand {
	
	public void execute(RoverDriver roverDriver) {
		roverDriver.turnLeft();
	}
	
}
