package com.xing.hiring.marsrover.command;

import com.xing.hiring.marsrover.model.RoverDriver;

public class RightCommand implements ICommand {

	public void execute(RoverDriver roverDriver) {
		roverDriver.turnRight();
	}

}
