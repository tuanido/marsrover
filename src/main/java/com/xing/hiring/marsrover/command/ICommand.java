package com.xing.hiring.marsrover.command;

import com.xing.hiring.marsrover.model.RoverDriver;

public interface ICommand {
	
	public void execute(RoverDriver roverDriver);

}
