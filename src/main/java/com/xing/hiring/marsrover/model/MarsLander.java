package com.xing.hiring.marsrover.model;

import java.util.List;

import com.xing.hiring.marsrover.command.Commander;

public class MarsLander {
	
	private Plateau plateau;
	private List<Commander> commanders;
	
	public MarsLander(Plateau plateau, List<Commander> commanders) {
		this.commanders = commanders;
		this.plateau = plateau;
	}
	
	public void land() {
		for (Commander commander : commanders) {
			commander.runCommands();
			plateau.addRover(commander.getRover());
		}
	}

	public void printLanding() {
		System.out.println(this.plateau);
	}

}
