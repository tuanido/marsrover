package com.xing.hiring.marsrover.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Plateau {
	
	private Coordinates originCoordinates;
	private Coordinates endCoordinates;
	private Map<Coordinates, Rover> roversLanded;
	
	public Plateau(Coordinates originCoordinates, Coordinates endCoordinates) {
		this.originCoordinates = originCoordinates;
		this.endCoordinates = endCoordinates;
		this.roversLanded = new HashMap<Coordinates, Rover>();
	}
	
	public boolean isInBounds (Coordinates coordinates) {
		boolean isInBounds = getOriginCoordinates().getX() <= coordinates.getX() && 
								getEndCoordinates().getX() >= coordinates.getX() &&
								getOriginCoordinates().getY() <= coordinates.getY() &&
								getEndCoordinates().getY() >= coordinates.getY();
		return isInBounds;
	}
	
	public void addRover (Rover rover) {
		this.roversLanded.put(rover.getCoordinates(), rover);
	}
	
	public Set<Coordinates> getRoversLandenCoordinates () {
		return this.roversLanded.keySet();
	}
	

	public Coordinates getOriginCoordinates() {
		return originCoordinates;
	}

	public void setOriginCoordinates(Coordinates originCoordinates) {
		this.originCoordinates = originCoordinates;
	}

	public Coordinates getEndCoordinates() {
		return endCoordinates;
	}

	public void setEndCoordinates(Coordinates endCoordinates) {
		this.endCoordinates = endCoordinates;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(System.lineSeparator()).append("Landed on a plateau of ");
		builder.append(this.endCoordinates.getX()).append(" X ");
		builder.append(this.endCoordinates.getY()).append(System.lineSeparator());
		builder.append("The rovers are in this coordinates:").append(System.lineSeparator());
		for (Rover rover : this.roversLanded.values()) {
			builder.append(rover.toString());
			builder.append(System.lineSeparator());
		}
		return builder.toString();
	}

}
