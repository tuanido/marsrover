package com.xing.hiring.marsrover.model;

public class RoverDriver {
	
	private Rover rover;
	private Plateau plateau;
	
	public RoverDriver(Rover rover, Plateau plateau) {
		this.plateau = plateau;
		this.rover = rover;
	}
	
	public void turnLeft () {
		this.rover.left();
	}
	
	public void turnRight () {
		this.rover.right();
	}
	
	public void move () {
		if (!isInColisions())
			this.rover.move();
	}
	
	private boolean isInColisions () {
		Coordinates nextPosition = this.rover.getNextPosition();
		boolean isInCollision = false;
		for (Coordinates occupiedCoord : this.plateau.getRoversLandenCoordinates()) {
			if (occupiedCoord.equals(nextPosition)) {
				isInCollision = true;
				break;
			}
		}
		return isInCollision || !this.plateau.isInBounds(nextPosition);
	}
	
	public Rover getRover() {
		return this.rover;
	}
	
	
}
