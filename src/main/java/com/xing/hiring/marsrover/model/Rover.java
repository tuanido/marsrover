package com.xing.hiring.marsrover.model;

public class Rover {
	
	private Coordinates coordinates;
	private Direction direction;
	
	public Rover(Coordinates coordinates, Direction direction) {
		this.coordinates = coordinates;
		this.direction = direction;
	}
	
	public void move() {
		//TODO check for collisions
		Coordinates newPosition = getNextPosition();
		this.setCoordinates(newPosition);
	}
	
	public void left() {
		this.direction = direction.getLeft();
	}
	
	public void right() {
		this.direction = direction.getRight();
	}
	
	public Coordinates getNextPosition() {
		return new Coordinates(	getCoordinates().getX() + getDirection().getShiftXAxis(),
				getCoordinates().getY() + getDirection().getShiftYAxis());
	}

	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}

	public Coordinates getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(Coordinates coordinates) {
		this.coordinates = coordinates;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Coords: [").append(this.getCoordinates().getX());
		builder.append(" ").append(this.getCoordinates().getY()).append("]");
		builder.append(" Direction: ").append(this.getDirection().name());
		return builder.toString();
	}

}
