package com.xing.hiring.marsrover.model;

public enum Direction {
	
	N	(0, 1) {
		@Override
		public Direction getLeft() {
			return W;
		}

		@Override
		public Direction getRight() {
			return E;
		}
	},
	S	(0, -1) {
		@Override
		public Direction getLeft() {
			return E;
		}

		@Override
		public Direction getRight() {
			return W;
		}
	},
	E	(1, 0) {
		@Override
		public Direction getLeft() {
			return N;
		}

		@Override
		public Direction getRight() {
			return S;
		}
	},
	W	(-1, 0) {
		@Override
		public Direction getLeft() {
			return S;
		}

		@Override
		public Direction getRight() {
			return N;
		}
	};
	
	private int shiftXAxis;
	private int shiftYAxis;
	
	private Direction (int x, int y) {
		this.shiftXAxis = x;
		this.shiftYAxis = y;
	}
	
	public int getShiftXAxis() {
		return this.shiftXAxis;
	}

	public int getShiftYAxis() {
		return this.shiftYAxis;
	}
	
	public static Direction findDirectionByName(String directionName) {
		for (Direction direction: values()) {
			if (direction.name().equals(directionName)) {
				return direction;
			}
		}
		return null;
	}
	
	public abstract Direction getLeft();
	public abstract Direction getRight();
}
