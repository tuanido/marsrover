package com.xing.hiring.marsrover.configuration;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public abstract class FileLoader {

private List<String> fileLines;
	
	public FileLoader(String configurationPath) {
		this.fileLines = new ArrayList<String>();
		loadConfigurationFile(configurationPath);
	}
	
	private void loadConfigurationFile(String configurationPath) {
		try(BufferedReader br = new BufferedReader(new FileReader(configurationPath))) {
			String line;
			while ((line = br.readLine()) != null) {
				this.fileLines.add(line);
		    }
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
	
	protected String getFileLine(int lineNum) {
		return this.fileLines.get(lineNum);
	}
	
	protected int getNumLinesInFile() {
		return this.fileLines.size();
	}
}
