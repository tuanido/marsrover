package com.xing.hiring.marsrover.configuration;

import java.util.ArrayList;
import java.util.List;

import com.xing.hiring.marsrover.command.Commander;
import com.xing.hiring.marsrover.command.ICommand;
import com.xing.hiring.marsrover.decoder.CommandDecoder;
import com.xing.hiring.marsrover.decoder.PlateauDecoder;
import com.xing.hiring.marsrover.decoder.RoverDecoder;
import com.xing.hiring.marsrover.model.Plateau;
import com.xing.hiring.marsrover.model.Rover;
import com.xing.hiring.marsrover.model.RoverDriver;

public class ConfigurationParser extends FileLoader {

	public static final int PLATEAU_CONFIG_LINE = 0;
	
	private Plateau plateau;
	private List<Commander> commanders;

	public ConfigurationParser(String configurationPath) {
		super(configurationPath);
	}

	public Plateau getPlateau() {
		if (this.plateau == null) {
			this.plateau = PlateauDecoder.getPlateau(getFileLine(PLATEAU_CONFIG_LINE));
		}
		return this.plateau;
	}
	
	public List<Commander> getCommanders () {
		if (this.commanders == null) {
			this.commanders = new ArrayList<Commander>();
			
			//Gets lines two by two starting from the second one.
			for (int i = 1; i < getNumLinesInFile(); i += 2) {
				RoverDriver roverDriver = createRoverDriverFromString(getFileLine(i));
				Commander commander = createCommanderFromString(getFileLine(i + 1));
				commander.setRoverDriver(roverDriver);
				this.commanders.add(commander);
			}
		}
		return this.commanders;
	}
	
	private RoverDriver createRoverDriverFromString (String roverDataString) {
		Rover rover = RoverDecoder.getRoverFromString(roverDataString);
		return new RoverDriver(rover, getPlateau());
	}
	
	private Commander createCommanderFromString(String commanderDataString) {
		List<ICommand> commands = CommandDecoder.getCommandList(commanderDataString);
		Commander commander = new Commander();
		commander.setCommands(commands);
		return commander;
	}
	
}
