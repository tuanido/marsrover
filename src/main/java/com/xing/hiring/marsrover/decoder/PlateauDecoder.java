package com.xing.hiring.marsrover.decoder;

import com.xing.hiring.marsrover.model.Coordinates;
import com.xing.hiring.marsrover.model.Plateau;

public class PlateauDecoder extends Decoder {

	private static final int COORDINATE_X_POSITION = 0;

	private static final int COORDINATE_Y_POSITION = 1;
	
	private static final Coordinates ORIGIN_COORDINATES = new Coordinates (0, 0);
	
	
	public static Plateau getPlateau(String plateauString) {
		return new Plateau(ORIGIN_COORDINATES, getCoordinatesFromString(plateauString));
	}
	
	private static Coordinates getCoordinatesFromString (String coordinatesString) {
		String cleanedCoordsString = cleanWhitesFromString(coordinatesString);
		String[] coordsStringArray = getCharactersAsStringArray(cleanedCoordsString);
		Integer coordX = Integer.parseInt(coordsStringArray[COORDINATE_X_POSITION]);
		Integer coordY = Integer.parseInt(coordsStringArray[COORDINATE_Y_POSITION]);
		return new Coordinates(coordX, coordY);
	}

}
