package com.xing.hiring.marsrover.decoder;

import com.xing.hiring.marsrover.model.Coordinates;
import com.xing.hiring.marsrover.model.Direction;
import com.xing.hiring.marsrover.model.Rover;

public class RoverDecoder extends Decoder {
	
	private static final int COORDINATE_X_POSITION = 0;

	private static final int COORDINATE_Y_POSITION = 1;

	private static final int DIRECTION_NAME_POSITION = 2;
	
	public static Rover getRoverFromString (String roverString) {
		String cleanedString = cleanWhitesFromString(roverString);
		Direction direction = getDirectionFromString(cleanedString);
		Coordinates coordinates = gerCoordintesFromString(cleanedString);
		return new Rover(coordinates, direction);
	}

	private static Coordinates gerCoordintesFromString(String cleanedString) {
		String[] charArray = getCharactersAsStringArray(cleanedString);
		Integer coordX = Integer.parseInt(charArray[COORDINATE_X_POSITION]);
		Integer coordY = Integer.parseInt(charArray[COORDINATE_Y_POSITION]);
		return new Coordinates(coordX, coordY);
	}

	private static Direction getDirectionFromString(String cleanedString) {
		String[] charArray = getCharactersAsStringArray(cleanedString.toUpperCase());
		return Direction.findDirectionByName(charArray[DIRECTION_NAME_POSITION]);
	}

}
