package com.xing.hiring.marsrover.decoder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.xing.hiring.marsrover.command.ICommand;
import com.xing.hiring.marsrover.command.LeftCommand;
import com.xing.hiring.marsrover.command.MoveCommand;
import com.xing.hiring.marsrover.command.RightCommand;

public class CommandDecoder extends Decoder {
	
	
	private static final Map<String, ICommand> commandParserMap = new HashMap<String, ICommand>() {
		private static final long serialVersionUID = -5596928142813466448L;
	{
		put ("M", new MoveCommand());
		put ("R", new RightCommand());
		put ("L", new LeftCommand());
	}};
	
	public static List<ICommand> getCommandList (String commandString) {
		String cleanedCommandString = cleanWhitesFromString(commandString);
		List<ICommand> commands = new ArrayList<ICommand>();
		for (String character : getCharactersAsStringArray(cleanedCommandString.toUpperCase())) {
			ICommand command = getCommandByCharacter(character);
			commands.add(command);
		}
		return commands;
	}
	
	private static ICommand getCommandByCharacter(String character) {
		return commandParserMap.get(character);
	}

}
