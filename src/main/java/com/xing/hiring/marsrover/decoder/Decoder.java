package com.xing.hiring.marsrover.decoder;

import java.util.Arrays;

public abstract class Decoder {

	private static final String CHARACTER_SPLIT_REGEX = "";
	private static final int START_INDEX = 1;
	private static final String CLEAN_WHITES_REGEX = "\\s";
	private static final String WHITES_REPLACEMENT = "";
	
	protected static String[] getCharactersAsStringArray (String string) {
		return Arrays.copyOfRange(string.split(CHARACTER_SPLIT_REGEX), START_INDEX, string.length() + 1);
	}
	
	protected static String cleanWhitesFromString (String stringWithWhites) {
		return stringWithWhites.replaceAll(CLEAN_WHITES_REGEX, WHITES_REPLACEMENT);
	}

}
