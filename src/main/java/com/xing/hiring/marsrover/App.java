package com.xing.hiring.marsrover;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.xing.hiring.marsrover.configuration.ConfigurationParser;
import com.xing.hiring.marsrover.model.MarsLander;

public class App {
	
	private static final String EXAMPLE_FILE_NAME = "marsrover.conf";

	public static void main (String[] args) {
		if (args.length < 1) {
			showUsage();
			return;
		}
		String filename = args[0];
		ConfigurationParser confLoader = new ConfigurationParser(filename);
		MarsLander marsLander = new MarsLander(confLoader.getPlateau(), confLoader.getCommanders());
		marsLander.land();
		marsLander.printLanding();
	}

	private static void showUsage() {
		System.out.println("Usage:");
		System.out.println();
		System.out.println("	java -jar marsrover.jar <Path to configuration file>");
		System.out.println();
		System.out.println("The configuration file has to be in the following format:");
		System.out.println();
		printExampleFile(EXAMPLE_FILE_NAME);
	}

	private static void printExampleFile(String filePath) {
		try (BufferedReader br = new BufferedReader(new InputStreamReader(App.class.getClassLoader().getResourceAsStream(EXAMPLE_FILE_NAME)))) {
			String line;
			while ((line = br.readLine()) != null) {
				System.out.println(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
