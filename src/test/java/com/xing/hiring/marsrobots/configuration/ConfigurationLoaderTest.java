package com.xing.hiring.marsrobots.configuration;

import java.net.URL;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.xing.hiring.marsrover.command.Commander;
import com.xing.hiring.marsrover.configuration.ConfigurationParser;
import com.xing.hiring.marsrover.model.Plateau;

public class ConfigurationLoaderTest {
	
	private static final String CONFIG_PATH = "./marsrover.conf";
	
	private ConfigurationParser configLoader;
	
	@Before
	public void init() {
		URL url = ConfigurationLoaderTest.class.getClassLoader().getResource(CONFIG_PATH);
		configLoader = new ConfigurationParser(url.getPath());
	}
	
	@Test
	public void getPlateauTest () {
		Plateau plateau = configLoader.getPlateau();
		Assert.assertEquals(5, plateau.getEndCoordinates().getX());
		Assert.assertEquals(5, plateau.getEndCoordinates().getY());
		
		List<Commander> commanders = configLoader.getCommanders();
		Assert.assertEquals(2, commanders.size());
		Assert.assertEquals(1, commanders.get(0).getRover().getCoordinates().getX());
		Assert.assertEquals(2, commanders.get(0).getRover().getCoordinates().getY());
		Assert.assertEquals(3, commanders.get(1).getRover().getCoordinates().getX());
		Assert.assertEquals(3, commanders.get(1).getRover().getCoordinates().getY());
	}
}
