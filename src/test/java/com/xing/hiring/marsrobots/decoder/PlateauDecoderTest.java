package com.xing.hiring.marsrobots.decoder;

import org.junit.Assert;
import org.junit.Test;

import com.xing.hiring.marsrover.decoder.PlateauDecoder;
import com.xing.hiring.marsrover.model.Plateau;

public class PlateauDecoderTest {
	
	@Test
	public void getPlateauTest () {
		String coordsText = " 5 5 ";
		Plateau plateau = PlateauDecoder.getPlateau(coordsText);
		Assert.assertEquals(5, plateau.getEndCoordinates().getX());
		Assert.assertEquals(5, plateau.getEndCoordinates().getY());
	}

}
