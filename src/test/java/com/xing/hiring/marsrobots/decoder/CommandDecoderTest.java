package com.xing.hiring.marsrobots.decoder;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.xing.hiring.marsrover.command.ICommand;
import com.xing.hiring.marsrover.command.LeftCommand;
import com.xing.hiring.marsrover.command.MoveCommand;
import com.xing.hiring.marsrover.command.RightCommand;
import com.xing.hiring.marsrover.decoder.CommandDecoder;

public class CommandDecoderTest {

	
	@Test
	public void getCommandListTest () {
		String commandString = " L R M ";
		List<ICommand> commands = CommandDecoder.getCommandList(commandString);
		Assert.assertTrue(commands.get(0) instanceof LeftCommand);
		Assert.assertTrue(commands.get(1) instanceof RightCommand);
		Assert.assertTrue(commands.get(2) instanceof MoveCommand);
	}
}
