package com.xing.hiring.marsrobots.decoder;

import org.junit.Assert;
import org.junit.Test;

import com.xing.hiring.marsrover.decoder.RoverDecoder;
import com.xing.hiring.marsrover.model.Rover;


public class RoberDecoderTest {
	
	
	@Test
	public void getRoverFromStringTest () {
		String roberText = "  4  5  N  ";
		Rover rover = RoverDecoder.getRoverFromString(roberText);
		
		Assert.assertEquals(4, rover.getCoordinates().getX());
		Assert.assertEquals(5, rover.getCoordinates().getY());
		Assert.assertEquals("N", rover.getDirection().name());
		
	}

}
