package com.xing.hiring.marsrobots.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.xing.hiring.marsrover.model.Coordinates;
import com.xing.hiring.marsrover.model.Direction;
import com.xing.hiring.marsrover.model.Plateau;
import com.xing.hiring.marsrover.model.Rover;
import com.xing.hiring.marsrover.model.RoverDriver;


public class RoverDriverTest {
	
	private static final Coordinates ORIGIN_PLANTEAU_COORDINATES = new Coordinates(0, 0);
	private static final Coordinates SIZE_PLANTEAU_COORDINATES = new Coordinates(5, 5);

	private static final Coordinates ROVER_COORDINATES = new Coordinates(3, 4);
	
	private Rover rover;
	private Plateau planteau;
	
	@Before
	public void init() {
		
		this.planteau = new Plateau(ORIGIN_PLANTEAU_COORDINATES, SIZE_PLANTEAU_COORDINATES);
		this.rover = new Rover(ROVER_COORDINATES, Direction.N);
		planteau.addRover(this.rover);
		
	}
	
	@Test
	public void isInCollisionTest () {
		Coordinates collisionCoordinates = new Coordinates(3, 3);
		Rover collisionRover = new Rover(collisionCoordinates, Direction.N);
		
		RoverDriver driver = new RoverDriver(collisionRover, this.planteau);
		driver.move();
		
		Assert.assertEquals(3, collisionRover.getCoordinates().getX());		
		Assert.assertEquals(3, collisionRover.getCoordinates().getY());
		
		driver.turnLeft();
		driver.move();
		driver.move();
		driver.move();
		driver.move();

		Assert.assertEquals(0, collisionRover.getCoordinates().getX());		
		Assert.assertEquals(3, collisionRover.getCoordinates().getY());
		
		
		
	}

}
