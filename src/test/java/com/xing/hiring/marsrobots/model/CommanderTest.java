package com.xing.hiring.marsrobots.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.xing.hiring.marsrover.command.Commander;
import com.xing.hiring.marsrover.decoder.CommandDecoder;
import com.xing.hiring.marsrover.model.Coordinates;
import com.xing.hiring.marsrover.model.Direction;
import com.xing.hiring.marsrover.model.Plateau;
import com.xing.hiring.marsrover.model.Rover;
import com.xing.hiring.marsrover.model.RoverDriver;

public class CommanderTest {
	
	public static final String COMMANDS= "LMLMRMMM";
	public static final Coordinates ROVER_COORDINATES = new Coordinates(2, 3);
	public static final Coordinates PLATEAU_SIZE = new Coordinates(6, 6);
	public static final Coordinates PLATEAU_ORIGIN = new Coordinates(0, 0);
	
	private RoverDriver roverDriver;
	
	@Before
	public void init() {
		this.roverDriver = new RoverDriver(new Rover(ROVER_COORDINATES, Direction.E), 
											new Plateau(PLATEAU_ORIGIN, PLATEAU_SIZE));
	}
	
	@Test
	public void runCommandsTest () {
		Commander commander = new Commander();
		commander.setCommands(CommandDecoder.getCommandList(COMMANDS));
		commander.setRoverDriver(roverDriver);
		commander.runCommands();
		Assert.assertEquals(1, commander.getRover().getCoordinates().getX());
		Assert.assertEquals(6, commander.getRover().getCoordinates().getY());
		Assert.assertEquals("N", commander.getRover().getDirection().name());
	}
	
}
