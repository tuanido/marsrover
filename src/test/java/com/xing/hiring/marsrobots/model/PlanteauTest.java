package com.xing.hiring.marsrobots.model;

import org.junit.Assert;
import org.junit.Test;

import com.xing.hiring.marsrover.model.Coordinates;
import com.xing.hiring.marsrover.model.Plateau;

public class PlanteauTest {
	
	@Test
	public void isIndBoundsTest () {
		Coordinates originCoordinates = new Coordinates(0, 0);
		Coordinates endCoordinates = new Coordinates(5, 6);
		Plateau planteau = new Plateau(originCoordinates, endCoordinates);
		Coordinates pointInside = new Coordinates(2, 4);
		Coordinates pointOrigin = new Coordinates(0, 0);
		Coordinates pointEnd = new Coordinates(5, 6);
		Coordinates pointOutside = new Coordinates(6, 7);
		Assert.assertTrue(planteau.isInBounds(pointInside));
		Assert.assertTrue(planteau.isInBounds(pointOrigin));
		Assert.assertTrue(planteau.isInBounds(pointEnd));
		Assert.assertFalse(planteau.isInBounds(pointOutside));
	}
}
