package com.xing.hiring.marsrobots.commands;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.xing.hiring.marsrover.command.ICommand;
import com.xing.hiring.marsrover.command.LeftCommand;
import com.xing.hiring.marsrover.command.MoveCommand;
import com.xing.hiring.marsrover.command.RightCommand;
import com.xing.hiring.marsrover.model.Coordinates;
import com.xing.hiring.marsrover.model.Direction;
import com.xing.hiring.marsrover.model.Plateau;
import com.xing.hiring.marsrover.model.Rover;
import com.xing.hiring.marsrover.model.RoverDriver;


public class CommandsTest {
	
	private Plateau plateau;
	
	@Before
	public void init () {
		this.plateau = new Plateau(new Coordinates(0, 0), new Coordinates(5, 5));
	}
	
	@Test
	public void rightCommandTest() {
		Rover robot = createRobot(3, 2, Direction.N);
		RoverDriver driver = new RoverDriver(robot, this.plateau);
		ICommand rightCommand = new RightCommand();
		rightCommand.execute(driver);
		assertEquals(Direction.E, robot.getDirection());
		rightCommand.execute(driver);
		assertEquals(Direction.S, robot.getDirection());
		rightCommand.execute(driver);
		assertEquals(Direction.W, robot.getDirection());
		rightCommand.execute(driver);
		assertEquals(Direction.N, robot.getDirection());
		
	}
	
	@Test
	public void leftCommandTest() {
		Rover robot = createRobot(3, 2, Direction.N);
		RoverDriver driver = new RoverDriver(robot, this.plateau);
		ICommand leftCommand = new LeftCommand();
		leftCommand.execute(driver);
		assertEquals(Direction.W, robot.getDirection());
		leftCommand.execute(driver);
		assertEquals(Direction.S, robot.getDirection());
		leftCommand.execute(driver);
		assertEquals(Direction.E, robot.getDirection());
		leftCommand.execute(driver);
		assertEquals(Direction.N, robot.getDirection());
	}
	
	@Test
	public void moveCommandTest() {
		Rover robot = createRobot(3, 2, Direction.N);
		RoverDriver driver = new RoverDriver(robot, this.plateau);
		ICommand moveCommand = new MoveCommand();
		ICommand leftCommand = new LeftCommand();
		ICommand rightCommand = new RightCommand();
		moveCommand.execute(driver);
		assertEquals(3, robot.getCoordinates().getX(), 0);
		assertEquals(3, robot.getCoordinates().getY(), 0);
		
		leftCommand.execute(driver);
		assertEquals(Direction.W, robot.getDirection());
		
		moveCommand.execute(driver);
		assertEquals(2, robot.getCoordinates().getX(), 0);
		assertEquals(3, robot.getCoordinates().getY(), 0);
		
		rightCommand.execute(driver);
		assertEquals(Direction.N, robot.getDirection());
		
		moveCommand.execute(driver);
		assertEquals(2, robot.getCoordinates().getX(), 0);
		assertEquals(4, robot.getCoordinates().getY(), 0);
		
		
	}
	
	public static Rover createRobot (int x, int y, Direction direction) {
		Coordinates roverCoords = new Coordinates(x, y);
		return new Rover(roverCoords, direction);
	}
	

}
