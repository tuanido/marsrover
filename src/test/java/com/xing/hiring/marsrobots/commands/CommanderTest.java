package com.xing.hiring.marsrobots.commands;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.xing.hiring.marsrover.command.Commander;
import com.xing.hiring.marsrover.command.ICommand;
import com.xing.hiring.marsrover.command.LeftCommand;
import com.xing.hiring.marsrover.command.MoveCommand;
import com.xing.hiring.marsrover.command.RightCommand;
import com.xing.hiring.marsrover.model.Coordinates;
import com.xing.hiring.marsrover.model.Direction;
import com.xing.hiring.marsrover.model.Plateau;
import com.xing.hiring.marsrover.model.Rover;
import com.xing.hiring.marsrover.model.RoverDriver;

public class CommanderTest {

	private static final Coordinates ORIGIN_PLATEAU_COORDINATES = new Coordinates(0, 0);
	private static final Coordinates SIZE_PLATEAU_COORDINATES = new Coordinates(5, 5);

	private static final Coordinates ROVER_COORDINATES = new Coordinates(3, 4);
	
	private Rover rover;
	private RoverDriver roverDriver;
	private Plateau plateau;
	
	
	@Before
	public void init() {
		this.rover = new Rover(ROVER_COORDINATES, Direction.N);
		this.plateau = new Plateau(ORIGIN_PLATEAU_COORDINATES, SIZE_PLATEAU_COORDINATES);
		this.roverDriver = new RoverDriver(rover, plateau);
	}
	
	
	@Test
	public void runCommandsTest () {
		List<ICommand> commands = new ArrayList<ICommand>();
		commands.add(new LeftCommand());	// 3 4 W
		commands.add(new LeftCommand());	// 3 4 S
		commands.add(new RightCommand());	// 3 4 W
		commands.add(new MoveCommand());	// 2 4 W
		commands.add(new MoveCommand());	// 1 4 W
		commands.add(new MoveCommand());	// 0 4 W
		
		Commander commander = new Commander();
		commander.setRoverDriver(this.roverDriver);
		commander.setCommands(commands);
		commander.runCommands();
		
		Assert.assertEquals(0, this.rover.getCoordinates().getX());
		Assert.assertEquals(4, this.rover.getCoordinates().getY());
		Assert.assertEquals("W", this.rover.getDirection().name());
		
		
	}

}
