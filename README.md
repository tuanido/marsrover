# MARS ROVER #

This repository is to solve the Mars Rover problem.

### HOW TO SET UP ###

This repo is builded with maven. To install it:

* Clone it into your computer.

	`$ git clone git@bitbucket.org:tuanido/marsrover.git`

* CD into the directory and type:

	`$ mvn package`

* The .jar file will be into the target directory.

### HOW TO RUN IT? ###

* You have to provide a configuration file with the following format:

> 5 5

> 1 2 N

> LMLMLMLMM

> 3 3 E

> MMRMMRMRRM

* To execute in a console just type: 

	`$ java -jar marsrover.jar <path_to_the_configuration_file>`